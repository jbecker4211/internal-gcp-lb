output "selflinkhealth" {
  description = "self link to health check for backend service"
  value       = "${google_compute_health_check.backend-health-check.self_link}"
}
