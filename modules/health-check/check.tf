resource "google_compute_health_check" "backend-health-check" {
  project = "${var.project}"

  name               = "backend-service-health-check"
  timeout_sec        = "1"
  check_interval_sec = "1"

  tcp_health_check {
    port = "80"
  }
}
