resource "google_compute_project_metadata_item" "k0" {
  project = "cr-lab-jbecker-0512181639"
  key     = "k0_ingress_address"
  value   = "${google_compute_address.clusterA-static-ip.address}"
}

resource "google_compute_project_metadata_item" "k1" {
  project = "cr-lab-jbecker-0512181639"
  key     = "k1_ingress_address"
  value   = "${google_compute_address.clusterB-static-ip.address}"
}
