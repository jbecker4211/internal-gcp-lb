variable "clusterA_name" {
  description = "Cluster A name for static IP for the ingress controller"
  type        = "string"
  default     = "k0"
}

variable "clusterB_name" {
  description = "Cluster B name for static IP for the ingress controller"
  type        = "string"
  default     = "k1"
}
