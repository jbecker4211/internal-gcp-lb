output "clusterA-static-ip" {
  value       = "google_compute_address.clusterA-static-ip.address"
  description = "Static IP for the Cluster A ingress controller"
}

output "clusterB-static-ip" {
  value       = "google_compute_address.clusterB-static-ip.address"
  description = "Static IP for the Cluster A ingress controller"
}
