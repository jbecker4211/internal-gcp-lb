resource "google_compute_address" "clusterA-static-ip" {
  name         = "${var.clusterA_name}"
  address_type = "INTERNAL"
  subnetwork   = "production-us-east1-5"
  project      = "cr-lab-jbecker-0512181639"
}

resource "google_compute_address" "clusterB-static-ip" {
  name         = "${var.clusterB_name}"
  address_type = "INTERNAL"
  subnetwork   = "production-us-east1-2"
  project      = "cr-lab-jbecker-0512181639"
}
