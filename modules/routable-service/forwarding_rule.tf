resource "google_compute_forwarding_rule" "default" {
  name        = "${var.forward_name}"
  description = "internal forwarding rule to create routable backend service"
  project     = "${var.project}"
  region      = "${var.region}"

  ### NETWORKING INFORMATION 
  ip_address            = "${google_compute_address.static-frontend-address.address}"
  ports                 = "${var.forward_port_list}"
  subnetwork            = "${var.ip_address_subnetwork}"
  network_tier          = "PREMIUM"
  ip_protocol           = "TCP"
  load_balancing_scheme = "INTERNAL"

  ### BACKEND CONFIG
  backend_service = "${google_compute_region_backend_service.default.self_link}"

  ### LABELS
  labels = "${var.forward_label_list}"
}
