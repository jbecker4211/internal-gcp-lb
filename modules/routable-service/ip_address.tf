data "google_compute_subnetwork" "ip-network" {
  project = "${var.project}"
  region  = "${var.region}"
  name    = "${var.ip_address_subnetwork}"
}

resource "google_compute_address" "static-frontend-address" {
  name         = "${var.ip_address_name}"
  description  = "static IP for forwarding rule"
  address_type = "INTERNAL"
  subnetwork   = "${data.google_compute_subnetwork.ip-network.self_link}"
  labels       = "${var.ip_address_labels}"
  region       = "${var.region}"
  project      = "${var.project}"
}
