resource "google_compute_region_backend_service" "default" {
  lifecycle {
    create_before_destroy = "true"
  }

  name        = "${var.backend_name}"
  description = "backend service for application"
  project     = "${var.project}"

  timeout_sec = "${var.backend_timeout}"

  protocol                        = "HTTP"
  timeout_sec                     = "${var.backend_timeout}"
  session_affinity                = "${var.backend_session_affinity}"
  region                          = "${var.region}"
  connection_draining_timeout_sec = "${var.backend_draining_timout}"

  backend {
    group = "${var.backend_group_1}"
  }

  backend {
    group = "${var.backend_group_2}"
  }

  health_checks = ["${var.backend_service_health_checks}"]
}
