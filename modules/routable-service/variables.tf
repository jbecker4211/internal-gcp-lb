####
#### variables for forwarding rule
####

variable "forward_name" {
  type        = "string"
  description = "name of forwarding rule"
}

variable "forward_port_list" {
  type        = "list"
  description = "comma separated list of ports for the forwarding rule to use with backend service"
  default     = ["80"]
}

variable "forward_label_list" {
  type        = "list"
  description = "list of labels for forwarding rule"
  default     = []
}

variable "region" {
  type        = "string"
  description = "region to deploy to"
}

variable "project" {
  type = "string"

  description = "project to deploy forwarding rule to"
}

####
#### Varibales for backend service
####

variable "backend_name" {
  type        = "string"
  description = "name of backend service"
}

variable "backend_timeout" {
  type        = "string"
  description = "timeout in seconds for backend service to consider backend request failed."
  default     = "10"
}

variable "backend_session_affinity" {
  type        = "string"
  description = "session affinity for backend service"
  default     = "NONE"
}

variable "backend_draining_timout" {
  type        = "string"
  description = "time for which instance will be drainined"
  default     = "0"
}

variable "backend_group_1" {
  type        = "string"
  description = "instnace group that this backend service attaches too"
}

variable "backend_group_2" {
  type        = "string"
  description = "second backend group for redundancy purposes"
}

variable "backend_service_health_checks" {
  type        = "list"
  description = "health checks for instances"
}

####
#### Variables for static IP address for forwarding rule
####

variable "ip_address_name" {
  type        = "string"
  description = "name of static ip address"
}

variable "ip_address_subnetwork" {
  type        = "string"
  description = "subnetwork to pull internal IP from"
}

variable "ip_address_labels" {
  type        = "list"
  description = "list of labels for IP address resource"
}
