##THIS IS A TEST REPO THE VALUES HARD CODED ARE SPECIFIC TO A SPECIFIC LAB


###How To Use
- run terraform init and apply from deploy/provision-ips directory
- run terraform init and apply from deploy/main-deploy directory
- TODO NOT COMPLETE OTHER STEPS NEED TO BE ADDED

###Current State:
- hardcoded variables
- integrated startup scripts.
- health checks are working
- frontend is complete
- private IPs are reserved and stored in metadata
- instance groups stand up and configure properly
- 2 private clusters are deployed 
- resources for kubnerentes demo service available
- test endpoints working



###TODOs:
- add dns entry for resolution
- un-hardcode variables that will need to change
- add SSL to NGINX config
- python script to test endpoints continuosly that can be extended to test other services once ready.
- need to make sure everything works from scratch and identify gaps in code and actual infrastructure



![diagram](files/picture.png)
