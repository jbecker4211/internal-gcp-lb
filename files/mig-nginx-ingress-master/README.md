# Status
Working, see TODOs.

To test: SSH to bastion and run `curl 10.1.0.7`

See instance template tyler-mig-21dec-717pm-1

# TODO

* Is ingress IP internal or external - script will need changes if internal as expected
* Remove workaround noted as WHILE TESTING when using 2+ cpu instance
* Consider a different way to get ingress controller IP
    * Cloud function
    * Rely on metadata update?
    * [Use nginx plus for redirection](https://www.nginx.com/blog/dynamic-reconfiguration-with-nginx-plus/)
        * would need DNS record to associate with LB IP
        * K8s does not apear to provide this so some other sort of DNS record would need to be maintained/updated with cloud function
        * Least intrusive - no change to startup script or nginx config if IP changes
* Harden nginx config

* Used gcloud command inside the MIG startup script to grab the static IPs to place inside of the NGINX config.
* Startup script installs nginx and writes config to specified location.  Script has place holders for where variable would go if address isn't defined before hand. Details in the startup script comments.


