#!/bin/bash

set -ex

############################################################################
#  Install NGINX and grab static IPs using gcloud
############################################################################

apt-get update -y && apt-get install -y nginx
CLUSTER_1_STATIC_IP="cluster1-ingress-controller"  #Metadata name of ingress controller static ip
CLUSTER1_ADDRESS=$(curl -H "Metadata-Flavor: Google" http://metadata.google.internal/computeMetadata/v1/project/attributes/cluster1-ingress-controller)
echo ${CLUSTER1_ADDRESS}

CLUSTER_2_STATIC_IP="cluster2-ingress-controller" #Replace with $2 when you know the name of the variable and pass it in. Or terraform.
CLUSTER2_ADDRESS=$(curl -H "Metadata-Flavor: Google" http://metadata.google.internal/computeMetadata/v1/project/attributes/cluster2-ingress-controller)
echo ${CLUSTER2_ADDRESS}

############################################################################
#  Write NGINX config
############################################################################

cat <<EOF > /etc/nginx/nginx.conf
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
    worker_connections 768;
        # multi_accept on;
}

http {

    #
    # Basic Settings
    ##

    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 65;
    types_hash_max_size 2048;
    # server_tokens off;

    # server_names_hash_bucket_size 64;
    # server_name_in_redirect off;
    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    ##
    # SSL Settings
    ##

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
    ssl_prefer_server_ciphers on;

    ##
    # Logging Settings
    ##

    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    ##
    # Gzip Settings
    ##

    gzip on;
    gzip_disable "msie6";

    # gzip_vary on;
    # gzip_proxied any;
    # gzip_comp_level 6;
    # gzip_buffers 16 8k;
    # gzip_http_version 1.1;
    # gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

    ##
    # Virtual Host Configs
    ##

    #include /etc/nginx/conf.d/*.conf;
    #include /etc/nginx/sites-enabled/*;
}

    include /etc/nginx/conf.d/*.conf;

#mail {
#    # See sample authentication script at:
#    # http://wiki.nginx.org/ImapAuthenticateWithApachePhpScript
#
#    # auth_http localhost/auth.php;
#    # pop3_capabilities "TOP" "USER";
#    # imap_capabilities "IMAP4rev1" "UIDPLUS";
#
#    server {
#        listen     localhost:110;
#        protocol   pop3;
#        proxy      on;
#    }
#
#    server {
#        listen     localhost:143;
#        protocol   imap;
#        proxy      on;
#    }
#}
EOF


cat <<EOF > /etc/nginx/conf.d/ingress.conf
stream {
 upstream backend_nodes {
   server $CLUSTER1_ADDRESS:80;
   server $CLUSTER2_ADDRESS:80;
}
 upstream backend_nodes_ssl {
   server $CLUSTER1_ADDRESS:443;
   server $CLUSTER2_ADDRESS:443;
}
server {
    listen 80;
    proxy_pass backend_nodes;
}
server {
    listen 443;
    proxy_pass backend_nodes_ssl;
  }
}
EOF

################################################################################
# Reload nginx config and make sure it's running
################################################################################

systemctl enable nginx
systemctl restart nginx
