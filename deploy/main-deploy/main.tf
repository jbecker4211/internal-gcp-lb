provider "google" {
  credentials = "${file("~/account.json")}"
}

data "template_file" "mig-startup" {
  template = "${file("${format("%s/startup-script/startup.sh",path.module)}")}"
}

module "mig-1" {
  source         = "../../modules/instance-group"
  startup_script = "${data.template_file.mig-startup.rendered}"
  network        = "prod-network"
  subnetwork     = "production-us-east1-8"
  name           = "nginx-mig-1"
  size           = "3"
  can_ip_forward = "true"
  compute_image  = "ubuntu-os-cloud/ubuntu-1604-lts"

  target_tags = ["http-server", "https-server"]

  service_port      = "80"
  service_port_name = "web-server"

  disk_size_gb      = "10"
  http_health_check = "false"

  project = "cr-lab-jbecker-0512181639"
}

module "mig-2" {
  source         = "../../modules/instance-group"
  startup_script = "${data.template_file.mig-startup.rendered}"
  network        = "prod-network"
  subnetwork     = "production-us-east1-8"
  name           = "nginx-mig-2"
  size           = "3"
  can_ip_forward = "true"
  compute_image  = "ubuntu-os-cloud/ubuntu-1604-lts"

  target_tags = ["http-server", "https-server"]

  service_port      = "80"
  service_port_name = "web-server"

  disk_size_gb      = "10"
  http_health_check = "false"

  project = "cr-lab-jbecker-0512181639"
}

module "front-end" {
  source                        = "../../modules/routable-service"
  forward_name                  = "forward-rule-1"
  forward_port_list             = ["80"]
  ip_address_subnetwork         = "production-us-east1-8"
  region                        = "us-east1"
  project                       = "cr-lab-jbecker-0512181639"
  backend_name                  = "test-backend"
  backend_group_1               = "${module.mig-1.regional_mig_instance_group}"
  backend_group_2               = "${module.mig-2.regional_mig_instance_group}"
  backend_service_health_checks = ["${module.health.selflinkhealth}"]
  ip_address_name               = "ip-address-for-front"
  ip_address_labels             = []
  project                       = "cr-lab-jbecker-0512181639"
}

module "health" {
  source  = "../../modules/health-check"
  project = "cr-lab-jbecker-0512181639"
}
