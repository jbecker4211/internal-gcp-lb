provider "google" {
  credentials = "${file("~/account.json")}"
  region      = "us-east1"
}

module "ips" {
  source = "../../modules/static-ips"
}
